//soal 1
let panjang = 100;
let lebar = 40;

if (panjang === 100){
    let keliling = 2*(panjang+lebar);
    let luas = panjang*lebar;

    console.log(luas);
    console.log(keliling);

}

const persegipanjang = (panjang,lebar) => {
    console.log("Jika panjang = " + panjang, "dan lebar = " + lebar,", maka Luas = ",+ panjang*lebar,"dan Keliling = ",+ 2*(panjang+lebar));
};
persegipanjang(100,40)

//soal 2
const myFunction = (firstName,lastName) => {
    console.log( "" + firstName,"" + lastName);
};
myFunction("William","Imoh");

//soal 3
var numbers = ["Muhammad","Iqbal Mubarok","Jalan Ranamanyar","playing football"];
const [firstName, lastName, address, hobby] = numbers;
console.log(firstName, lastName, address, hobby);

//soal 4
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]
console.log(combined)

//soal 5
const planet = "earth";
const view  = "glass";

const before = {
    Lorem :view, dolor_sit_amet:view, consectetur_adipiscing_elit:planet
};

console.log(before);