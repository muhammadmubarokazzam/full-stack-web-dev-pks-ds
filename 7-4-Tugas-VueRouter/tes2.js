// Kita membuat 2 component , yaitu Home dan About
const Home = { template: '<div>Ini Halaman Home</div>' }
const About = { template: '<div>Ini Halaman About</div>' }

// Kemudian kita arahkan setiap route ke component yang kita inginkan
const routes = [
  { path: '/', component: Home , alias: '/home'},
  { path: '/about', component: About }
]

//Kemudian kita instansiasi object Vue Router dan tambahkan routes yang sudah kita buat tadi
const router = new VueRouter({
  routes 
})

//Kemudian kita tambahkan object VueRouter yang kita namakan router ke object Vue

var vm = new Vue({ 
   el: '#app', 
   router, 
  })

const routes = [
    { path: '/', component: Home, alias: '/home' },
    { path: '/about', component: About },
    { path: '*', redirect: '/' }
]

const routes = [
    { path: '/', component: Home, alias: '/home' },
    { path: '/about', component: About },
    { path: '/categories', component: CategoriesComponent },
    { path: '*', redirect: '/' }
]

const routes = [
    { path: '/', component: Home, alias: '/home' },
    { path: '/about', component: About },
    { path: '/categories', component: CategoriesComponent },
    { path: '/category/:id', component: CategoryComponent },
    { path: '*', redirect: '/' }

]