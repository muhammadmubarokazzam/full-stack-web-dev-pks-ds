
//var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

//************************ARRAY************************


//console.log(daftarHewan[0])
//console.log(daftarHewan[daftarHewan.length -5])


//push
//daftarHewan.push("sleeping","swimming")
//console.log(daftarHewan) //'2. Komodo','5. Buaya','3. Cicak','4. Ular','1. Tokek','sleeping','swimming'


//pop
//daftarHewan.pop()
//console.log(daftarHewan) //'2. Komodo', '5. Buaya', '3. Cicak', '4. Ular'


//unshift
//daftarHewan.unshift("0. Biawak")
//console.log(daftarHewan) //'0. Biawak','2. Komodo','5. Buaya','3. Cicak','4. Ular','1. Tokek'


//shift
//daftarHewan.shift("0. Biawak")
//console.log(daftarHewan) //'5. Buaya', '3. Cicak', '4. Ular', '1. Tokek'


//sort
//daftarHewan.sort()
//console.log(daftarHewan) //'1. Tokek', '2. Komodo', '3. Cicak', '4. Ular', '5. Buaya'


//slice, tdk akan merubah array pembangunnya
//var irisan0 = daftarHewan.slice(2)
//console.log(irisan0) //'3. Cicak', '4. Ular', '1. Tokek'

//splice
//daftarHewan.splice(1,1,"0. Biawak")
//console.log(daftarHewan) //'2. Komodo', '0. Biawak', '3. Cicak', '4. Ular', '1. Tokek'


//split
//var daftarHewan1 = daftarHewan[1]
//var name = daftarHewan1.trim().split(".")
//console.log(name) //'5', ' Buaya'


//join
//var satu = daftarHewan.join("$")
//console.log(satu) //2. Komodo$5. Buaya$3. Cicak$4. Ular$1. Tokek


//************************FUNCTION*********************


//function tampilkan(){ //daftarHewan tdk bisa karena tidak akan terbaca
    //console.log(daftarHewan)
//}
//tampilkan() //'2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek'


//function jumlah(a,b,c){
    //return a * b - c
//}
//console.log(jumlah(5,1,8)); //-3


//function operasi(a,b,c,model = "penjumlahan"){
    //var d
    //if(model == "penjumlahan"){
        //d = a + b + c
    //}

    //else if(model == "pengurangan"){
        //d = a - b - c
    //}

    //return d
//}
//console.log(operasi(5,1,8)); //14
//console.log(operasi(9,2,3,"pengurangan")); //4


//***********************OBJECT************************


//mirip asosiatif array di php

//var hewan = {
    //ganassatu : "5. Buaya",
    //ganasdua : "4. Ular",
    //nempeldua : "1. Tokek",
//}
//hewan.ganas = "2. Komodo"
//hewan["nempelsatu"] = "3. Cicak"

//console.log(hewan.ganas) //2. Komodo
//console.log(hewan['nempelsatu']) //3. Cicak


//var cars = [
    //{merk: "BMW", warna: "merah", tipe: "sedan"}, 
    //{merk: "toyota", warna: "hitam", tipe: "box"}, 
    //{merk: "audi", warna: "biru", tipe: "sedan"}
//]


    //foreach
//cars.forEach(function(item){
    //item.tipe = "box"
//})
//console.log(cars)


    //map
//var colors = cars.map(function(car){
   //return car.warna
//})

//var newCars = cars.map(function(car1){
    //return car1.tipe = "sedan"
//})

//console.log(colors)
//console.log(newCars)

//fungsi npm (packace manajer) seperti komposer



//SOAL 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var daftarHewan1 = daftarHewan.sort()

for (var key in daftarHewan1){
    console.log(daftarHewan1[key])
}
console.log()

//SOAL 2
var data = {
    name : "John" , 
    age : 30 , 
    address : "Jalan Pelesiran" , 
    hobby : "Gaming" 
}

var data1 = data
var data1 = ["Nama saya ",age,", ","umur saya ",age," tahun, alamat saya di ",address,", ","dan saya punya hobby yaitu ",hobby]

console.log(data)
console.log()

//SOAL 3


var hitung_1 = ("Muhammad")

var hitung_2 = ("Iqbal")
var name1 = hitung_1.split("a")
var name2 = hitung_2.split("b")


console.log(name1.length , name2.length) // 3 2
console.log()

//SOAL 4
function hitung($angka){
    var a = $angka - 2 + $angka
    return a
}
console.log(hitung(0))
console.log(hitung(1))
console.log(hitung(2))
console.log(hitung(3))
console.log(hitung(5))
